import expressLoader from './express';
import logger from './logger';
import express from 'express';
import config from '../config';

export const app = express();

export default async () => {

    /**
     * Port loader
     */
    await app.listen(config.port || 5000, () => {
        try {
            logger.info(`
        ################################################
        🛡️  Server listening on port: ${config.port} 🛡️ 
        ################################################
      `);
        }
        catch (err) {
            logger.error(err);
            process.exit(1);
        }
    });

    /**
     * MongoDB loader, creates mongoClient and connect to the db and return db connection.
     */
    //const mongoConnection = await mongooseLoader();
    //logger.info('✌️ DB loaded and connected!');

    /**
     * Laods express essentials 
     */
    await expressLoader({ app });
    logger.log("info", "Express Loader has initalized successfully! ✅");


};