import { createConnection, Connection } from 'typeorm';
import logger from '../logger';


const connection = createConnection({
    "type": "mysql",
    "host": "127.0.0.1",
    "port": 3306,
    "username": "admin",
    "password": "admin",
    "database": "tno_DB",
    "entities": ["package/program/app/domain/entities/*.ts"]
}
).then(connection => {
    logger.info('✌️ DB MySQL loaded and connected !');
})
    .catch(error => { logger.error(error) })

if (connection === undefined) {
    throw new Error('Error connecting to database');
} // In case the connection failed, the app stops.

    //connection.synchronize(); // this updates the database schema to match the models' definitions

    //this.connection = connection; // Store the connection object in the class instance.



