//BOOTSTRAP PATHS

import { ProgramMysqlRepository } from '../app/infrastructure/program.mysql.repository';

import { CreateProgramController } from '../app/application/controller/createProgramController'
import { ProgramCreator } from '../app/application/usecases/ProgramCreator'

// BOOTSTRAP SERVICES
/**
 * Aquí se decide cuál implementación de las
 * diferentes infraestructuras se va a utilizar.
 */
const programRepository = new ProgramMysqlRepository();



// BOOTSTRAP COMMANDS  (USES CASE)
const createProgram = new ProgramCreator({ programRepository });


// BOOTSTRAP CONTROLLERS
const programCreatorController = new CreateProgramController({ createProgram })





export {
    programCreatorController
};
