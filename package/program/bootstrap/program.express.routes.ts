/**
 * Routes to User Service.
 */
import express from 'express';

import {
    programCreatorController
} from "./bootstrap";

var router = express.Router();

router.post("/api/programas", programCreatorController.handle.bind(programCreatorController));


export default router;


