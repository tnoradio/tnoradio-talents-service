import { Program } from '../../domain/entities/Program';
import Command from '../command';
import { ProgramRepository } from '../../domain/services/program.service.repository';

interface Services {
    programRepository: ProgramRepository;
}

export class ProgramCreator extends Command {

    private _program: Program;

    constructor(private services: Services) {
        super();
    }

    public programCreate(program: Program) {
        this._program = program;
    }

    public getprogramCreate() {
        return this._program;
    }

    //  Override Method
    public async execute() {
        const response = await this.services.programRepository.createProgram(this._program);
        return response;
    }


}