import e from 'express';
import { ProgramCreator } from '../usecases/ProgramCreator';


interface UsesCases {
    createProgram: ProgramCreator;
}

export class CreateProgramController {

    constructor(private usescases: UsesCases) {
    }

    async handle(req: e.Request, res: e.Response) {

        this.usescases.createProgram.programCreate(req.body);

        try {
            const program = await this.usescases.createProgram.execute();

            return res.status(201).send(program);
        } catch (error) {
            console.log(error);
            return res.status(400).send(error);
        }


    }
}