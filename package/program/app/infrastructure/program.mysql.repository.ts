import { Program } from '../domain/entities/Program';
import { ProgramRepository } from '../domain/services/program.service.repository'
import { getManager } from 'typeorm'

export class ProgramMysqlRepository implements ProgramRepository {
    createProgram(program: Program): Promise<Program | Error> {
        try {
            return getManager().getRepository(Program).save(program);
        } catch (error) {
            return error
        }
    }
    getAllPrograms(): Promise<Error | Program[]> {
        throw new Error('Method not implemented.');
    }

}