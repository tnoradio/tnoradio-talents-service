import { Entity, Column, PrimaryGeneratedColumn, Timestamp } from 'typeorm';

@Entity()
export class Program {
    @PrimaryGeneratedColumn('uuid')
    pro_id: string;

    @Column({
        type: 'varchar',
        //nullable: false,
        length: 50,
        unique: true
    })
    name: string;

    @Column({
        type: 'varchar',
        //nullable: false
    })
    sinopsis: string;

    @Column({
        type: 'time'
    })
    start_time: Timestamp;

    @Column({
        type: 'time'
    })
    end_time: Timestamp


    @Column({
        type: 'date'
    })
    date_origin: Date

    @Column({
        type: 'varchar',
        length: 100
    })
    email: string

    @Column({
        type: 'varchar',
        default: 'Producer name',
        length: 100
    })
    producer: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    instagram: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    twitter: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    facebook: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    snapchat: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    updatedBy: string;

    @Column({
        type: 'boolean'
    })
    isEnabled: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    weekday: string[];

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    announcer_1: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    announcer_2: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    announcer_3: string;

    @Column({
        type: 'varchar',
        length: 100,
        //nullable: true
    })
    announcer_4: string;

}

    //$table -> string('pro_corte') ->default ('Salud');

