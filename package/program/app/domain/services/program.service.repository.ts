import { Program } from '../entities/Program';

export interface ProgramRepository {
  createProgram(program: Program): Promise<Program | Error>;
  getAllPrograms(): Promise<Program[] | Error>;
}